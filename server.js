const express = require("express");
const app = express();

import bodyParser from 'body-parser';
import { getStudentList, findStudentById } from "./student";
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const studentList = getStudentList();


app.get("/", (req, res) => {
    res.send("Welcome to Test Liquid Consulting API");
});


/* GET Students */
app.get("/students", (req, res) => {
    return res.status(200).send({
        success: "true",
        message: "Estudiantes",
        payload: studentList,
    });
});


/* GET Student */
app.get("/getStudent/:id", (req, res) => {
    const studentId = parseInt(req.params.id, 10);

    for(let i = 0; i < studentList.length; i++){
        if(studentList[i].student_id === studentId){
            const selectedStudent = studentList[i];
            return res.status(200).send({
                success: 'true',
                message: 'Estudiante',
                payload: selectedStudent
            });
        }
    }
    return res.status(404).send({
        success: 'true',
        message: 'Error en la busqueda'
    });
});

/* ADD Student */
app.post("/addStudent", (req, res) => {

    if (!req.body.first_name) {
        return res.status(400).send({
            success: "false",
            message: "El nombre es requerido",
        });
    } else if (!req.body.last_name) {
        return res.status(400).send({
            success: "false",
            message: "El apellido es requerido",
        });
    } else if (!req.body.date_of_birth) {
        return res.status(400).send({
            success: "false",
            message: "La fecha de nacimiento es requerida",
        });
    } else if (!req.body.gender) {
        return res.status(400).send({
            success: "false",
            message: "El genero es requerido",
        });
    } else if (!req.body.university) {
        return res.status(400).send({
            success: "false",
            message: "La universidad es requerida",
        });
    } else if (!req.body.is_active) {
        return res.status(400).send({
            success: "false",
            message: "El estado es requerido",
        });
    }

    const maxStudentId = getLast(studentList).student_id;
    const newStudent = {
        student_id: maxStudentId + 1,
        first_name:  req.body.first_name,
        last_name: req.body.last_name,
        date_of_birth: req.body.date_of_birth,
        gender: req.body.gender,
        university: req.body.university,
        major: req.body.major,
        date_of_admission: req.body.date_of_admission,
        address: req.body.address,
        is_active: (req.body.is_active === 'true')
    };
    studentList.push(newStudent);
    return res.status(201).send({
        success: "true",
        message: "El estudiante fue ingresado exitosamente",
        newStudent,
    });
});

let getLast =  (arr = null, n = null) => {
    if (arr == null) return void 0;
    if (n === null) return arr[arr.length - 1];
    return arr.slice(Math.max(arr.length - n, 0));
};

/* UPDATE Student */
app.put("/updateStudent/:id", (req, res) => {
    const studentId = parseInt(req.params.id, 10);
    const studentFound = findStudentById(studentId);

    if (!studentFound) {
        return res.status(404).send({
            success: 'false',
            message: 'El estudiante no fue encontrado',
        });
    }

    if (!req.body.first_name) {
        return res.status(400).send({
            success: "false",
            message: "El nombre es requerido",
        });
    } else if (!req.body.last_name) {
        return res.status(400).send({
            success: "false",
            message: "El apellido es requerido",
        });
    } else if (!req.body.date_of_birth) {
        return res.status(400).send({
            success: "false",
            message: "La fecha de nacimiento es requerida",
        });
    } else if (!req.body.gender) {
        return res.status(400).send({
            success: "false",
            message: "El genero es requerido",
        });
    } else if (!req.body.university) {
        return res.status(400).send({
            success: "false",
            message: "La universidad es requerida",
        });
    } else if (!req.body.is_active) {
        return res.status(400).send({
            success: "false",
            message: "El estado es requerido",
        });
    }

    const updatedStudent = {
        student_id: studentId,
        first_name:  req.body.first_name,
        last_name: req.body.last_name,
        date_of_birth: req.body.date_of_birth,
        gender: req.body.gender,
        university: req.body.university,
        major: req.body.major,
        date_of_admission: req.body.date_of_admission,
        address: req.body.address,
        is_active: (req.body.is_active === 'true')
    };

    for (let i = 0; i < studentList.length; i++) {
        if (studentList[i].student_id === studentId) {
            studentList[i] = updatedStudent;
            return res.status(201).send({
                success: 'true',
                message: 'El estudiante fue actualizado exitosamente',
                updatedStudent
            });
        }
    }
    return  res.status(404).send({
        success: 'true',
        message: 'Error en la actualización'

    });
});


/* DELETE Student */
app.delete("/deleteStudent/:id", (req, res) => {
    const studentId = parseInt(req.params.id, 10);
    for(let i = 0; i < studentList.length; i++){
        if(studentList[i].student_id === studentId){
            studentList.splice(i,1);
            return res.status(201).send({
                success: 'true',
                message: 'El estudiante fue removido exitosamente',
                payload: 'Removed'
            });
        }
    }
    return res.status(404).send({
        success: 'true',
        message: 'Error en la remoción'
    });
});


app.listen(3000, () => {
    console.log("Server listening on port 3000");
});

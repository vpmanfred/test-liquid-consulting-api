export const getStudentList = () => {
    return [
        {
            "student_id": 1,
            "first_name": "Joe",
            "last_name": "Doe",
            "date_of_birth": "1980-01-23",
            "gender": "male",
            "university": "University 10",
            "major": "Informatica",
            "date_of_admission": "2017-10-01",
            "address": "San José",
            "is_active": true
        },
        {
            "student_id": 2,
            "first_name": "María",
            "last_name": "Perez",
            "date_of_birth": "1981-02-24",
            "gender": "female",
            "university": "University 9",
            "major": "Administracion",
            "date_of_admission": "2018-09-01",
            "address": "Heredia",
            "is_active": false
        },
        {
            "student_id": 3,
            "first_name": "Juan",
            "last_name": "Monge",
            "date_of_birth": "1982-03-25",
            "gender": "male",
            "university": "University 8",
            "major": "Medicina",
            "date_of_admission": "2015-11-01",
            "address": "Limón",
            "is_active": true
        },
        {
            "student_id": 4,
            "first_name": "Pedro",
            "last_name": "Alvarez",
            "date_of_birth": "1983-04-26",
            "gender": "male",
            "university": "University 7",
            "major": "Contabilidad",
            "date_of_admission": "2013-12-01",
            "address": "Puntarenas",
            "is_active": false
        },
        {
            "student_id": 5,
            "first_name": "Andrea",
            "last_name": "Quintana",
            "date_of_birth": "1984-05-27",
            "gender": "female",
            "university": "University 6",
            "major": "Odontologia",
            "date_of_admission": "2014-02-01",
            "address": "Cartago",
            "is_active": true
        },
        {
            "student_id": 6,
            "first_name": "Mario",
            "last_name": "Sosa",
            "date_of_birth": "1985-06-28",
            "gender": "male",
            "university": "University 5",
            "major": "Informatica",
            "date_of_admission": "2019-04-01",
            "address": "Puntarenas",
            "is_active": false
        },
        {
            "student_id": 7,
            "first_name": "Luisa",
            "last_name": "Chavarria",
            "date_of_birth": "1986-07-29",
            "gender": "female",
            "university": "University 4",
            "major": "Administracion",
            "date_of_admission": "2018-03-01",
            "address": "Guanacaste",
            "is_active": true
        },
        {
            "student_id": 8,
            "first_name": "Pepe",
            "last_name": "Flores",
            "date_of_birth": "1987-08-01",
            "gender": "male",
            "university": "University 3",
            "major": "Medicina",
            "date_of_admission": "2017-06-01",
            "address": "San José",
            "is_active": false
        },
        {
            "student_id": 9,
            "first_name": "Gabriela",
            "last_name": "Chinchilla",
            "date_of_birth": "1988-09-13",
            "gender": "female",
            "university": "University 2",
            "major": "Contabilidad",
            "date_of_admission": "2016-07-01",
            "address": "Heredia",
            "is_active": true
        },
        {
            "student_id": 10,
            "first_name": "Alex",
            "last_name": "Rojas",
            "date_of_birth": "1989-10-18",
            "gender": "male",
            "university": "University 1",
            "major": "Odontologia",
            "date_of_admission": "2014-08-01",
            "address": "Cartago",
            "is_active": false
        }
    ];
};

export const findStudentById = (studentId) =>{
    const students = getStudentList()
    const studentFound = students.filter((student) => {
        if (student.student_id === studentId) {
            return student
        }
    });
    if(studentFound.length>0){
        return studentFound
    }
    return false
};
